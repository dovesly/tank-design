package com.tank.war;

/**
 * @ClassName TankMian
 * @Description TODO
 * @Author SLy
 * @Date 2020/10/27 21:24
 * @Version 1.0
 **/
public class TankMian {
    public static void main(String[] args) throws InterruptedException {
        TankFrame tankFrame = TankFrame.getInstance();
        while (true) {
            Thread.sleep(50);
            tankFrame.repaint();
        }
    }
}
