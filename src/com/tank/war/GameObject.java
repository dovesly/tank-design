package com.tank.war;

import java.awt.*;

/**
 * @ClassName GameObject
 * @Description TODO
 * @Author SLy
 * @Date 2020/12/7 22:30
 * @Version 1.0
 **/
public abstract class GameObject {

    int x, y;

    abstract void paint(Graphics g);
}
