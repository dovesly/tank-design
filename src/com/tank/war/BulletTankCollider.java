package com.tank.war;

import java.awt.*;

/**
 * @ClassName BulletTankCollider
 * @Description TODO
 * @Author SLy
 * @Date 2020/12/7 22:49
 * @Version 1.0
 **/
public class BulletTankCollider implements Collider {

    @Override
    public boolean collide(GameObject o1, GameObject o2) {
        if (o1 instanceof GoodBullet && o2 instanceof BadTank) {
            GoodBullet goodBullet = (GoodBullet) o1;
            BadTank badTank = (BadTank) o2;

            Rectangle weaponRect = new Rectangle(goodBullet.x, goodBullet.y, GoodBullet.GOOD_BULLET_WIDTH, GoodBullet.GOOD_BULLET_HEIGHT);
            Rectangle vehicleRect = new Rectangle(badTank.x, badTank.y, BadTank.BAD_TANK_WIDTH, BadTank.BAD_TANK_HEIGHT);
            if (weaponRect.intersects(vehicleRect)) {
                goodBullet.die();
                badTank.die();
                GameModel.getInstance().add(new Explode((badTank.x + (badTank.width / 2 - Explode.EXPLODE_WIDTH / 2)), (badTank.y + (badTank.height / 2 - Explode.EXPLODE_HEIGHT / 2))));
                return false;
            }
            return true;

        } else if (o1 instanceof BadTank && o2 instanceof GoodBullet) {
            return collide(o2, o1);
        }
        return true;
    }
}
