package com.tank.war;

import java.io.IOException;
import java.util.Properties;

/**
 * @ClassName PropertiesMgr
 * @Description TODO
 * @Author SLy
 * @Date 2020/11/1 21:32
 * @Version 1.0
 **/
public class PropertiesMgr {

    private static final PropertiesMgr propertiesMgr = new PropertiesMgr();

    private static Properties properties = new Properties();

    private PropertiesMgr() {
    }

    public static PropertiesMgr getInstance() {
        return propertiesMgr;
    }

    static {
        try {
            properties.load(PropertiesMgr.class.getClassLoader().getResourceAsStream("config"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Object get(String key) {
        if (properties == null) return null;
        return properties.get(key);
    }

}
