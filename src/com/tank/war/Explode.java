package com.tank.war;

import java.awt.*;

/**
 * @ClassName Explode
 * @Description TODO
 * @Author SLy
 * @Date 2020/10/31 14:37
 * @Version 1.0
 **/
public class Explode extends GameObject{

    public static final int EXPLODE_WIDTH = ResourceMgr.getInstance().getExplodeList()[0].getWidth(null);
    public static final int EXPLODE_HEIGHT = ResourceMgr.getInstance().getExplodeList()[0].getHeight(null);

    private int x = 200, y = 200;

    private boolean living = true;

    private int step = 0;

    public Explode(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void paint(Graphics g) {
        g.drawImage(ResourceMgr.getInstance().getExplodeList()[step++], x, y, null);
        if (step >= ResourceMgr.getInstance().getExplodeList().length)
            GameModel.getInstance().remove(this);
    }
}
