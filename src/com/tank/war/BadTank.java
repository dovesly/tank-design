package com.tank.war;

import java.awt.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @ClassName Tank
 * @Description TODO
 * @Author SLy
 * @Date 2020/10/28 20:51
 * @Version 1.0
 **/
public class BadTank extends Vehicle {

    public static final int BAD_TANK_WIDTH = ResourceMgr.getInstance().getBadTankU().getWidth(null);

    public static final int BAD_TANK_HEIGHT = ResourceMgr.getInstance().getBadTankU().getHeight(null);

    private int oldX = 200, oldY = 200;

    public BadTank(int x, int y, DirectionEnum direction, TypeEnum type) {
        super.x = x;
        super.y = y;
        super.direction = direction;
        super.type = type;

        super.rect.x = x;
        super.rect.y = y;
        super.rect.width = ResourceMgr.getInstance().getBadTankU().getWidth(null);
        super.rect.height = ResourceMgr.getInstance().getBadTankU().getHeight(null);

        width = ResourceMgr.getInstance().getBadTankU().getWidth(null);
        height = ResourceMgr.getInstance().getBadTankU().getHeight(null);

        GameModel.getInstance().add(this);
    }

    @Override
    public void fire(FireStrategy strategyFire) {
        // 可以直接调用，这里为了学习反射使用反射来实现
        // strategyFire.fire(this);

        try {
            // 因为BadFireStrategy使用单例，所以使用暴力反射setAccessible(true);
            Class c = Class.forName((String) PropertiesMgr.getInstance().get("badStrategy"));
            Constructor constructor = c.getDeclaredConstructor();
            constructor.setAccessible(true);
            strategyFire = (FireStrategy) constructor.newInstance();
            strategyFire.fire(this);
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    @Override
    void paint(Graphics g) {
        if (!super.isLiving()) {
            GameModel.getInstance().remove(this);
        }
        switch (direction) {
            case UP:
                g.drawImage(ResourceMgr.getInstance().getBadTankU(), super.x, super.y, null);
                break;
            case DOWN:
                g.drawImage(ResourceMgr.getInstance().getBadTankD(), super.x, super.y, null);
                break;
            case LEFT:
                g.drawImage(ResourceMgr.getInstance().getBadTankL(), super.x, super.y, null);
                break;
            case RIGHT:
                g.drawImage(ResourceMgr.getInstance().getBadTankR(), super.x, super.y, null);
                break;
        }
        this.move();
    }

    @Override
    void move() {
        // 移动前记录之前的位置
        oldX = super.x;
        oldY = super.y;

        if (!super.moving) return;
        switch (direction) {
            case LEFT:
                super.x -= SPEED;
                break;
            case RIGHT:
                super.x += SPEED;
                break;
            case UP:
                super.y -= SPEED;
                break;
            case DOWN:
                super.y += SPEED;
                break;
            default:
                break;
        }

        if (random.nextInt(100) > 95)
            this.fire(BadFireStrategy.getInstance());

        if (random.nextInt(100) > 95)
            super.randomDir();

        super.boundsCheck();
        super.rect.x = super.x;
        super.rect.y = super.y;
    }

    void back() {
        super.x = oldX;
        super.y = oldY;
    }
}
