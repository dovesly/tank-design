package com.tank.war;

import java.awt.*;
import java.util.Random;

/**
 * @ClassName Vehicle
 * @Description TODO
 * @Author SLy
 * @Date 2020/11/12 22:31
 * @Version 1.0
 **/
public abstract class Vehicle extends GameObject {

    int x = 200, y = 200;

    int width = 200, height = 200;

    boolean moving = true;

    private boolean living = true;

    TypeEnum type;

    DirectionEnum direction;

    Random random = new Random();

    Rectangle rect = new Rectangle();

    static final int SPEED = Integer.parseInt((String) PropertiesMgr.getInstance().get("tankSpeed"));

    abstract void fire(FireStrategy strategyFire);

    abstract void paint(Graphics g);

    abstract void move();

    void boundsCheck() {
        if (x < 2) x = 2;
        if (y < 28) y = 28;
        if (x > TankFrame.GAME_WIDTH - width - 2)
            x = TankFrame.GAME_WIDTH - width - 2;
        if (y > TankFrame.GAME_HEIGHT - height - 2)
            y = TankFrame.GAME_HEIGHT - height - 2;
    }

    void randomDir() {
        direction = DirectionEnum.values()[random.nextInt(4)];
    }

    void die() {
        living = false;
    }

    boolean isLiving() {
        return living;
    }

    TypeEnum getType() {
        return type;
    }

    void setMoving(boolean moving) {
        this.moving = moving;
    }

    void setDirection(DirectionEnum direction) {
        this.direction = direction;
    }

    DirectionEnum getDirection() {
        return direction;
    }

    Rectangle getRect() {
        return rect;
    }
}
