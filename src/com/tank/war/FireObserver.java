package com.tank.war;

/**
 * @ClassName ObserverFire
 * @Description TODO
 * @Author SLy
 * @Date 2020/12/18 20:59
 * @Version 1.0
 **/
public interface FireObserver {
    void actionOnFire(FireEvent event);
}
