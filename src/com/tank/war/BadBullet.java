package com.tank.war;

import java.awt.*;

/**
 * @ClassName Bullet
 * @Description TODO
 * @Author SLy
 * @Date 2020/10/28 21:34
 * @Version 1.0
 **/
public class BadBullet extends Weapon {

    public static final int BAD_BULLET_WIDTH = ResourceMgr.getInstance().getBadBulletU().getWidth(null);

    public static final int BAD_BULLET_HEIGHT = ResourceMgr.getInstance().getBadBulletU().getHeight(null);

    public BadBullet(int x, int y, DirectionEnum direction, TypeEnum type) {
        super.x = x;
        super.y = y;
        super.type = type;
        super.direction = direction;

        super.rect.x = x;
        super.rect.y = y;
        super.rect.width = ResourceMgr.getInstance().getBadBulletU().getWidth(null);
        super.rect.height = ResourceMgr.getInstance().getBadBulletU().getHeight(null);

        width = ResourceMgr.getInstance().getBadBulletU().getWidth(null);
        height = ResourceMgr.getInstance().getBadBulletU().getHeight(null);

        GameModel.getInstance().add(this);
    }

    @Override
    public void move() {
        living = true;
        switch (direction) {
            case LEFT:
                super.x -= SPEED;
                break;
            case RIGHT:
                super.x += SPEED;
                break;
            case UP:
                super.y -= SPEED;
                break;
            case DOWN:
                super.y += SPEED;
                break;
            default:
                break;
        }

        if (super.x < 0 || super.y < 0 || super.x > TankFrame.GAME_WIDTH || super.y > TankFrame.GAME_HEIGHT) {
            super.living = false;
        }

        super.rect.x = super.x;
        super.rect.y = super.y;
    }

    @Override
    void paint(Graphics g) {
        if (!super.isLiving()) {
            GameModel.getInstance().remove(this);
        }
        switch (direction) {
            case UP:
                g.drawImage(ResourceMgr.getInstance().getBadBulletU(), super.x, super.y, null);
                break;
            case DOWN:
                g.drawImage(ResourceMgr.getInstance().getBadBulletD(), super.x, super.y, null);
                break;
            case LEFT:
                g.drawImage(ResourceMgr.getInstance().getBadBulletL(), super.x, super.y, null);
                break;
            case RIGHT:
                g.drawImage(ResourceMgr.getInstance().getBadBulletR(), super.x, super.y, null);
                break;
        }
        this.move();
    }

}
