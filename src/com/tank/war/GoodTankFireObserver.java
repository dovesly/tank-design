package com.tank.war;

/**
 * @ClassName GoodTankObserverFire
 * @Description TODO
 * @Author SLy
 * @Date 2020/12/18 21:00
 * @Version 1.0
 **/
public class GoodTankFireObserver implements FireObserver {

    @Override
    public void actionOnFire(FireEvent event) {
        if (event.getSource() instanceof GoodTank) {
            event.getFireStrategy().fire((GoodTank) event.getSource());
        }
    }
}
