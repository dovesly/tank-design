package com.tank.war;

import java.util.LinkedList;
import java.util.List;

/**
 * @ClassName ColliderChain
 * @Description TODO
 * @Author SLy
 * @Date 2020/12/8 22:38
 * @Version 1.0
 **/
public class ColliderChain implements Collider {

    private List<Collider> colliders = new LinkedList<>();

    public ColliderChain() {
        add(new BulletTankCollider());
        add(new TankTankCollider());
        add((new TankWallCollider()));
        add(new WeaponWallCollider());
    }

    public void add(Collider c) {
        colliders.add(c);
    }

    @Override
    public boolean collide(GameObject o1, GameObject o2) {
        for (int i = 0; i < colliders.size(); i++) {
            if (!colliders.get(i).collide(o1, o2)) {
                return false;
            }
        }
        return true;
    }
}
