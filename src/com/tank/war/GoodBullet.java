package com.tank.war;

import java.awt.*;

/**
 * @ClassName Bullet
 * @Description TODO
 * @Author SLy
 * @Date 2020/10/28 21:34
 * @Version 1.0
 **/
public class GoodBullet extends Weapon {

    public static final int GOOD_BULLET_WIDTH = ResourceMgr.getInstance().getGoodBulletU().getWidth(null);

    public static final int GOOD_BULLET_HEIGHT = ResourceMgr.getInstance().getGoodBulletU().getHeight(null);

    public GoodBullet(int x, int y, DirectionEnum direction, TypeEnum type) {
        super.x = x;
        super.y = y;
        super.type = type;
        super.direction = direction;

        super.rect.x = x;
        super.rect.y = y;
        super.rect.width = ResourceMgr.getInstance().getGoodBulletU().getWidth(null);
        super.rect.height = ResourceMgr.getInstance().getGoodBulletU().getHeight(null);

        width = ResourceMgr.getInstance().getGoodBulletU().getWidth(null);
        height = ResourceMgr.getInstance().getGoodBulletU().getHeight(null);

        GameModel.getInstance().add(this);
    }

    @Override
    public void move() {
        living = true;
        switch (direction) {
            case LEFT:
                super.x -= SPEED;
                break;
            case RIGHT:
                super.x += SPEED;
                break;
            case UP:
                super.y -= SPEED;
                break;
            case DOWN:
                super.y += SPEED;
                break;
            default:
                break;
        }

        if (super.x < 0 || super.y < 0 || super.x > TankFrame.GAME_WIDTH || super.y > TankFrame.GAME_HEIGHT) {
            super.living = false;
        }

        super.rect.x = super.x;
        super.rect.y = super.y;
    }

    @Override
    void paint(Graphics g) {
        if (!super.isLiving()) {
            GameModel.getInstance().remove(this);
        }
        switch (direction) {
            case UP:
                g.drawImage(ResourceMgr.getInstance().getGoodBulletU(), super.x, super.y, null);
                break;
            case DOWN:
                g.drawImage(ResourceMgr.getInstance().getGoodBulletD(), super.x, super.y, null);
                break;
            case LEFT:
                g.drawImage(ResourceMgr.getInstance().getGoodBulletL(), super.x, super.y, null);
                break;
            case RIGHT:
                g.drawImage(ResourceMgr.getInstance().getGoodBulletR(), super.x, super.y, null);
                break;
        }
        this.move();
    }

}
