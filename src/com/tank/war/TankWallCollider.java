package com.tank.war;

/**
 * @ClassName BulletTankCollider
 * @Description TODO
 * @Author SLy
 * @Date 2020/12/7 22:49
 * @Version 1.0
 **/
public class TankWallCollider implements Collider {

    @Override
    public boolean collide(GameObject o1, GameObject o2) {
        if (o1 instanceof BadTank && o2 instanceof Wall) {
            BadTank t = (BadTank) o1;
            Wall w = (Wall) o2;
            if (t.getRect().intersects((w.getRect()))) {
                t.back();
            }
        } else if (o1 instanceof Wall && o2 instanceof Vehicle) {
            collide(o2, o1);
        }
        return true;
    }
}
