package com.tank.war;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @ClassName TankFrame
 * @Description TODO
 * @Author SLy
 * @Date 2020/10/27 21:25
 * @Version 1.0
 **/
public class TankFrame extends Frame {

    private static final TankFrame tankFrame = new TankFrame();

    private static GameModel gameModel = GameModel.getInstance();

    public static final int GAME_WIDTH = 1024;

    public static final int GAME_HEIGHT = 768;

    public static TankFrame getInstance() {
        return tankFrame;
    }

    private TankFrame() {
        this.setSize(GAME_WIDTH, GAME_HEIGHT);
        this.setTitle("Tank War");
        this.setResizable(false);
        this.setVisible(true);

        this.addKeyListener(new KeyAdapter() {
            boolean leftFlag = false;
            boolean rightFlag = false;
            boolean upFlag = false;
            boolean downFlag = false;

            @Override
            public void keyPressed(KeyEvent e) {
                int key = e.getKeyCode();
                switch (key) {
                    case KeyEvent.VK_LEFT:
                        leftFlag = true;
                        break;
                    case KeyEvent.VK_RIGHT:
                        rightFlag = true;
                        break;
                    case KeyEvent.VK_UP:
                        upFlag = true;
                        break;
                    case KeyEvent.VK_DOWN:
                        downFlag = true;
                        break;
                    default:
                        break;
                }
                new Thread(() -> new Audio("audio/tank_move.wav").play()).start();
                setTankDirection();
            }

            @Override
            public void keyReleased(KeyEvent e) {
                int key = e.getKeyCode();
                switch (key) {
                    case KeyEvent.VK_LEFT:
                        leftFlag = false;
                        break;
                    case KeyEvent.VK_RIGHT:
                        rightFlag = false;
                        break;
                    case KeyEvent.VK_UP:
                        upFlag = false;
                        break;
                    case KeyEvent.VK_DOWN:
                        downFlag = false;
                        break;
                    case KeyEvent.VK_SPACE:
                        gameModel.goodVehicle.fire(GoodFireStrategy.getInstance());
                        break;
                    default:
                        break;
                }
                setTankDirection();
            }

            void setTankDirection() {
                if (!leftFlag && !rightFlag && !upFlag && !downFlag) {
                    gameModel.goodVehicle.setMoving(false);
                } else {
                    if (leftFlag) gameModel.goodVehicle.setDirection(DirectionEnum.LEFT);
                    if (rightFlag) gameModel.goodVehicle.setDirection(DirectionEnum.RIGHT);
                    if (upFlag) gameModel.goodVehicle.setDirection(DirectionEnum.UP);
                    if (downFlag) gameModel.goodVehicle.setDirection(DirectionEnum.DOWN);
                    gameModel.goodVehicle.setMoving(true);
                }
            }
        });

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    @Override
    public void paint(Graphics g) {
        gameModel.paint(g);
    }

    Image offScreenImage = null;

    @Override
    public void update(Graphics g) {
        if (offScreenImage == null) {
            offScreenImage = this.createImage(GAME_WIDTH, GAME_HEIGHT);
        }
        Graphics gOffScreen = offScreenImage.getGraphics();
        Color c = gOffScreen.getColor();
        gOffScreen.setColor(Color.BLACK);
        gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        gOffScreen.setColor(c);
        paint(gOffScreen);
        g.drawImage(offScreenImage, 0, 0, null);

    }

}
