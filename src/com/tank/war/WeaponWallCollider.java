package com.tank.war;

/**
 * @ClassName BulletTankCollider
 * @Description TODO
 * @Author SLy
 * @Date 2020/12/7 22:49
 * @Version 1.0
 **/
public class WeaponWallCollider implements Collider {

    @Override
    public boolean collide(GameObject o1, GameObject o2) {
        if (o1 instanceof Weapon && o2 instanceof Wall) {
            Weapon p = (Weapon) o1;
            Wall w = (Wall) o2;
            if (p.getRect().intersects((w.getRect()))) {
                p.die();
            }
        } else if (o1 instanceof Wall && o2 instanceof Weapon) {
            collide(o2, o1);
        }
        return true;
    }
}
