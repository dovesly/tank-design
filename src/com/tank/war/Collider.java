package com.tank.war;

/**
 * @ClassName Collider
 * @Description TODO
 * @Author SLy
 * @Date 2020/12/7 22:30
 * @Version 1.0
 **/
public interface Collider {

    boolean collide(GameObject o1, GameObject o2);
}
