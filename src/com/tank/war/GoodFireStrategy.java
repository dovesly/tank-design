package com.tank.war;

/**
 * @ClassName GoodStrategyFire
 * @Description TODO
 * @Author SLy
 * @Date 2020/11/4 22:01
 * @Version 1.0
 **/
public class GoodFireStrategy implements FireStrategy {

    private static final GoodFireStrategy goodFireStrategy = new GoodFireStrategy();

    private GoodFireStrategy() {
    }

    public static GoodFireStrategy getInstance() {
        return goodFireStrategy;
    }

    @Override
    public void fire(Vehicle vehicle) {
        DirectionEnum dir = vehicle.direction;
        DirectionEnum[] dirs = DirectionEnum.values();
        for (int i = 0; i < dirs.length; i++) {
            vehicle.setDirection(dirs[i]);
            GoodTankFactory.getInstance().createWeapon(vehicle);
        }
        vehicle.setDirection(dir);
        new Thread(() -> new Audio("audio/tank_fire.wav").play()).start();
    }
}
