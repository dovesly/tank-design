package com.tank.war;

/**
 * @ClassName TankFactory
 * @Description TODO
 * @Author SLy
 * @Date 2020/11/12 22:40
 * @Version 1.0
 **/
public class BadTankFactory extends AbstractFactory {

    private static final BadTankFactory BAD_TANK_FACTORY = new BadTankFactory();

    private BadTankFactory() {
    }

    public static BadTankFactory getInstance() {
        return BAD_TANK_FACTORY;
    }

    @Override
    Vehicle createVehicle(int x, int y) {
        return new BadTank(x, y, DirectionEnum.UP, TypeEnum.BAD);
    }

    @Override
    void createWeapon(Vehicle vehicle) {
        int x = vehicle.x + (BadTank.BAD_TANK_WIDTH / 2 - BadBullet.BAD_BULLET_WIDTH / 2);
        int y = vehicle.y + (BadTank.BAD_TANK_HEIGHT / 2 - BadBullet.BAD_BULLET_HEIGHT / 2);
        new BadBullet(x, y, vehicle.direction, TypeEnum.BAD);
    }
}
