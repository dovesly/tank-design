package com.tank.war;

import java.awt.*;

/**
 * @ClassName Wall
 * @Description TODO
 * @Author SLy
 * @Date 2020/12/10 20:19
 * @Version 1.0
 **/
public class Wall extends GameObject {

    int w, h;

    Rectangle rect;

    public Wall(int x, int y, int w, int h) {
        super.x = x;
        super.y = y;
        this.w = w;
        this.h = h;
        this.rect = new Rectangle(x, y, w, h);

        GameModel.getInstance().add(this);
    }

    @Override
    void paint(Graphics g) {
        Color c = g.getColor();
        g.setColor(Color.DARK_GRAY);
        g.fillRect(x, y, w, h);
        g.setColor(c);
    }

    public Rectangle getRect() {
        return rect;
    }
}
