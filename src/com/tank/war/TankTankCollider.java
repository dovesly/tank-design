package com.tank.war;

/**
 * @ClassName BulletTankCollider
 * @Description TODO
 * @Author SLy
 * @Date 2020/12/7 22:49
 * @Version 1.0
 **/
public class TankTankCollider implements Collider {

    @Override
    public boolean collide(GameObject o1, GameObject o2) {
        if (o1 instanceof BadTank && o2 instanceof BadTank) {
            BadTank t1 = (BadTank) o1;
            BadTank t2 = (BadTank) o2;
            if (t1.getRect().intersects((t2.getRect()))) {
                t1.back();
                t2.back();
            }
        }
        return true;
    }
}
