package com.tank.war;

/**
 * @ClassName StrategyFire
 * @Description TODO
 * @Author SLy
 * @Date 2020/11/4 22:00
 * @Version 1.0
 **/
public interface FireStrategy {

    void fire(Vehicle vehicle);
}
