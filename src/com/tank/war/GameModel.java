package com.tank.war;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName GameModel
 * @Description TODO
 * @Author SLy
 * @Date 2020/11/19 22:35
 * @Version 1.0
 **/
public class GameModel {
    private static final GameModel INSTANCE = new GameModel();

    private GameModel() {
    }

    public static GameModel getInstance() {
        return INSTANCE;
    }

    static {
        INSTANCE.init();
    }

    Vehicle goodVehicle;

    private void init() {
        // 初始化我军坦克
        goodVehicle = GoodTankFactory.getInstance().createVehicle(200, 400);

        // 初始化敌人坦克
        int tankCount = Integer.parseInt((String) PropertiesMgr.getInstance().get("badTankCount"));
        for (int i = 0; i < tankCount; i++) {
            BadTankFactory.getInstance().createVehicle(50 + i * 80, 200);
        }

        // 初始化墙
        new Wall(150, 150, 200, 50);
        new Wall(550, 150, 200, 50);
        new Wall(300, 300, 50, 200);
        new Wall(550, 300, 50, 200);
    }

    ColliderChain colliderChain = new ColliderChain();

    List<GameObject> gameObjects = new ArrayList<>();

    public void paint(Graphics g) {
        Color c = g.getColor();
        g.setColor(Color.WHITE);

        int bullet = 0, badTank = 0, explode = 0;
        for (GameObject go : gameObjects) {
            if (go instanceof Weapon) {
                bullet++;
            }
            if (go instanceof BadTank) {
                badTank++;
            }
            if (go instanceof Explode) {
                explode++;
            }
        }

        g.drawString("当前子弹的数量为：" + bullet, 10, 60);
        g.drawString("当前敌人的数量为：" + badTank, 10, 80);
        g.drawString("当前爆炸的数量为：" + explode, 10, 100);
        g.setColor(c);

        // 我军坦克
        goodVehicle.paint(g);

        // 游戏物体
        for (int i = 0; i < gameObjects.size(); i++) {
            gameObjects.get(i).paint(g);
        }

        // 碰撞检测
        for (int i = 0; i < gameObjects.size(); i++) {
            for (int j = i + 1; j < gameObjects.size(); j++) {
                colliderChain.collide(gameObjects.get(i), gameObjects.get(j));
            }
        }
    }

    public void add(GameObject go) {
        this.gameObjects.add(go);
    }

    public void remove(GameObject go) {
        this.gameObjects.remove(go);
    }
}
