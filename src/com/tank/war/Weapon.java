package com.tank.war;

import java.awt.*;

/**
 * @ClassName Weapon
 * @Description TODO
 * @Author SLy
 * @Date 2020/11/12 22:33
 * @Version 1.0
 **/
public abstract class Weapon extends GameObject {

    int x = 200, y = 200;

    int width = 200, height = 200;

    boolean living = true;

    TypeEnum type;

    DirectionEnum direction;

    Rectangle rect = new Rectangle();

    static final int SPEED = Integer.parseInt((String) PropertiesMgr.getInstance().get("bulletSpeed"));

    abstract void move();

    abstract void paint(Graphics g);

    void die() {
        living = false;
    }

    boolean isLiving() {
        return living;
    }

    TypeEnum getType() {
        return type;
    }

    public Rectangle getRect() {
        return rect;
    }
}
