package com.tank.war;

/**
 * @ClassName TankEvent
 * @Description TODO
 * @Author SLy
 * @Date 2020/12/18 21:03
 * @Version 1.0
 **/
public class FireEvent<T> {
    private T source;
    private FireStrategy fireStrategy;

    public FireEvent(T source, FireStrategy fireStrategy) {
        this.source = source;
        this.fireStrategy = fireStrategy;
    }

    public T getSource() {
        return source;
    }

    public void setSource(T source) {
        this.source = source;
    }

    public FireStrategy getFireStrategy() {
        return fireStrategy;
    }

    public void setFireStrategy(FireStrategy fireStrategy) {
        this.fireStrategy = fireStrategy;
    }
}
