package com.tank.war;

import java.awt.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @ClassName Tank
 * @Description TODO
 * @Author SLy
 * @Date 2020/10/28 20:51
 * @Version 1.0
 **/
public class GoodTank extends Vehicle {

    public static final int GOOD_TANK_WIDTH = ResourceMgr.getInstance().getGoodTankU().getWidth(null);

    public static final int GOOD_TANK_HEIGHT = ResourceMgr.getInstance().getGoodTankU().getHeight(null);

    public GoodTank(int x, int y, DirectionEnum direction, TypeEnum type) {
        super.x = x;
        super.y = y;
        super.direction = direction;
        super.type = type;

        super.rect.x = x;
        super.rect.y = y;
        super.rect.width = ResourceMgr.getInstance().getGoodTankU().getWidth(null);
        super.rect.height = ResourceMgr.getInstance().getGoodTankU().getHeight(null);

        width = ResourceMgr.getInstance().getGoodTankU().getWidth(null);
        height = ResourceMgr.getInstance().getGoodTankU().getHeight(null);
    }

    @Override
    public void fire(FireStrategy strategyFire) {
        // strategyFire.fire(this);
        this.handleFire(strategyFire);
    }

    // 观察者模式
    private List<FireObserver> observerFireList = Collections.singletonList(new GoodTankFireObserver());

    private void handleFire(FireStrategy strategyFire) {
        FireEvent<GoodTank> fireEvent = new FireEvent<>(this, strategyFire);
        for (FireObserver fireObserver : observerFireList) {
            fireObserver.actionOnFire(fireEvent);
        }
    }

    @Override
    void paint(Graphics g) {
        if (!super.isLiving()) {
            GameModel.getInstance().remove(this);
        }
        switch (direction) {
            case UP:
                g.drawImage(ResourceMgr.getInstance().getGoodTankU(), super.x, super.y, null);
                break;
            case DOWN:
                g.drawImage(ResourceMgr.getInstance().getGoodTankD(), super.x, super.y, null);
                break;
            case LEFT:
                g.drawImage(ResourceMgr.getInstance().getGoodTankL(), super.x, super.y, null);
                break;
            case RIGHT:
                g.drawImage(ResourceMgr.getInstance().getGoodTankR(), super.x, super.y, null);
                break;
        }
        this.move();
    }

    @Override
    void move() {
        if (!super.moving) return;
        switch (direction) {
            case LEFT:
                super.x -= SPEED;
                break;
            case RIGHT:
                super.x += SPEED;
                break;
            case UP:
                super.y -= SPEED;
                break;
            case DOWN:
                super.y += SPEED;
                break;
            default:
                break;
        }

        super.boundsCheck();
        super.rect.x = super.x;
        super.rect.y = super.y;
    }
}
