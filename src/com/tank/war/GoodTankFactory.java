package com.tank.war;

/**
 * @ClassName TankFactory
 * @Description TODO
 * @Author SLy
 * @Date 2020/11/12 22:40
 * @Version 1.0
 **/
public class GoodTankFactory extends AbstractFactory {
    private static final GoodTankFactory GOOD_TANK_FACTORY = new GoodTankFactory();

    private GoodTankFactory() {
    }

    public static GoodTankFactory getInstance() {
        return GOOD_TANK_FACTORY;
    }

    @Override
    Vehicle createVehicle(int x, int y) {
        return new GoodTank(x, y, DirectionEnum.UP, TypeEnum.GOOD);
    }

    @Override
    void createWeapon(Vehicle vehicle) {
        int x = vehicle.x + (GoodTank.GOOD_TANK_WIDTH / 2 - GoodBullet.GOOD_BULLET_WIDTH / 2);
        int y = vehicle.y + (GoodTank.GOOD_TANK_HEIGHT / 2 - GoodBullet.GOOD_BULLET_HEIGHT / 2);
        new GoodBullet(x, y, vehicle.direction, TypeEnum.GOOD);
    }
}
