package com.tank.war;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

/**
 * @ClassName ResourceMgr
 * @Description TODO
 * @Author SLy
 * @Date 2020/10/29 22:12
 * @Version 1.0
 **/
public class ResourceMgr {

    private static final ResourceMgr resourceMgr = new ResourceMgr();

    private static BufferedImage goodTankL, goodTankR, goodTankU, goodTankD;
    private static BufferedImage badTankL, badTankR, badTankU, badTankD;
    private static BufferedImage goodBulletL, goodBulletR, goodBulletU, goodBulletD;
    private static BufferedImage badBulletL, badBulletR, badBulletU, badBulletD;
    private static BufferedImage[] explodeList = new BufferedImage[16];

    private ResourceMgr() {
    }

    public static ResourceMgr getInstance() {
        return resourceMgr;
    }

    static {
        try {
            goodTankU = ImageIO.read(Objects.requireNonNull(ResourceMgr.class.getClassLoader().getResourceAsStream("images/GoodTank2.png")));
            goodTankL = ImageUtil.rotateImage(goodTankU, -90);
            goodTankR = ImageUtil.rotateImage(goodTankU, 90);
            goodTankD = ImageUtil.rotateImage(goodTankU, 180);

            badTankU = ImageIO.read(Objects.requireNonNull(ResourceMgr.class.getClassLoader().getResourceAsStream("images/BadTank2.png")));
            badTankL = ImageUtil.rotateImage(badTankU, -90);
            badTankR = ImageUtil.rotateImage(badTankU, 90);
            badTankD = ImageUtil.rotateImage(badTankU, 180);

            goodBulletU = ImageIO.read(Objects.requireNonNull(ResourceMgr.class.getClassLoader().getResourceAsStream("images/bulletU.png")));
            goodBulletL = ImageUtil.rotateImage(goodBulletU, -90);
            goodBulletR = ImageUtil.rotateImage(goodBulletU, 90);
            goodBulletD = ImageUtil.rotateImage(goodBulletU, 180);

            badBulletU = ImageIO.read(Objects.requireNonNull(ResourceMgr.class.getClassLoader().getResourceAsStream("images/bulletU.gif")));
            badBulletL = ImageUtil.rotateImage(badBulletU, -90);
            badBulletR = ImageUtil.rotateImage(badBulletU, 90);
            badBulletD = ImageUtil.rotateImage(badBulletU, 180);

            for (int i = 0; i < 16; i++) {
                explodeList[i] = ImageIO.read(Objects.requireNonNull(ResourceMgr.class.getClassLoader().getResourceAsStream("images/e" + (i + 1) + ".gif")));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BufferedImage getGoodTankL() {
        return goodTankL;
    }

    public BufferedImage getGoodTankR() {
        return goodTankR;
    }

    public BufferedImage getGoodTankU() {
        return goodTankU;
    }

    public BufferedImage getGoodTankD() {
        return goodTankD;
    }

    public BufferedImage getBadTankL() {
        return badTankL;
    }

    public BufferedImage getBadTankR() {
        return badTankR;
    }

    public BufferedImage getBadTankU() {
        return badTankU;
    }

    public BufferedImage getBadTankD() {
        return badTankD;
    }

    public BufferedImage getGoodBulletL() {
        return goodBulletL;
    }

    public BufferedImage getGoodBulletR() {
        return goodBulletR;
    }

    public BufferedImage getGoodBulletU() {
        return goodBulletU;
    }

    public BufferedImage getGoodBulletD() {
        return goodBulletD;
    }

    public BufferedImage getBadBulletL() {
        return badBulletL;
    }

    public BufferedImage getBadBulletR() {
        return badBulletR;
    }

    public BufferedImage getBadBulletU() {
        return badBulletU;
    }

    public BufferedImage getBadBulletD() {
        return badBulletD;
    }

    public BufferedImage[] getExplodeList() {
        return explodeList;
    }
}
