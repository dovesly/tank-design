package com.tank.war;

/**
 * @ClassName AbstractFacotory
 * @Description TODO
 * @Author SLy
 * @Date 2020/11/12 22:24
 * @Version 1.0
 **/
public abstract class AbstractFactory {

    abstract Vehicle createVehicle(int x, int y);

    abstract void createWeapon(Vehicle vehicle);
}
