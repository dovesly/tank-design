package com.tank.war;

/**
 * @ClassName GoodStrategyFire
 * @Description TODO
 * @Author SLy
 * @Date 2020/11/4 22:01
 * @Version 1.0
 **/
public class BadFireStrategy implements FireStrategy {
    private static final BadFireStrategy badFireStrategy = new BadFireStrategy();

    private BadFireStrategy() {
    }

    public static BadFireStrategy getInstance() {
        return badFireStrategy;
    }

    @Override
    public void fire(Vehicle vehicle) {
        BadTankFactory.getInstance().createWeapon(vehicle);
    }
}
